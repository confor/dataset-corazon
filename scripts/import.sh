#!/usr/bin/bash
set -eu

# uso:
#     ./import.sh | sqlite3 heart.db

[[ -f heart.db ]] && echo "advertencia: heart.db ya existe" >&2

# importar el csv
# columnas:
#     < ../schema.json jq --raw-output '.[] | .name + " " + (.type | ascii_upcase)'
# columnas para los datos normalizados:
#     < ../schema.json jq --raw-output '.[] | select(.range) | .name + "_norm DOUBLE NOT NULL,"'
cat <<EOF

BEGIN TRANSACTION;
PRAGMA automatic_index = TRUE;
DROP TABLE IF EXISTS original;

CREATE TABLE original (
	age INT NOT NULL,
	sex INT NOT NULL,
	cp INT NOT NULL,
	trtbps INT NOT NULL,
	chol INT NOT NULL,
	fbs BOOL NOT NULL,
	restecg INT NOT NULL,
	thalachh INT NOT NULL,
	exng INT NOT NULL,
	oldpeak DOUBLE NOT NULL,
	slp INT NOT NULL,
	caa INT NOT NULL,
	thall INT NOT NULL,
	output BOOL NOT NULL
);

.separator ,
.import --csv --skip 1 ../heart.csv original

CREATE TABLE heart (
	id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	age INT NOT NULL,
	age_norm DOUBLE NOT NULL,
	sex INT NOT NULL,
	cp INT NOT NULL,
	trtbps INT NOT NULL,
	trtbps_norm DOUBLE NOT NULL,
	chol INT NOT NULL,
	chol_norm DOUBLE NOT NULL,
	fbs BOOL NOT NULL,
	restecg INT NOT NULL,
	thalachh INT NOT NULL,
	thalachh_norm DOUBLE NOT NULL,
	exng INT NOT NULL,
	oldpeak DOUBLE NOT NULL,
	oldpeak_norm DOUBLE NOT NULL,
	slp INT NOT NULL,
	caa INT NOT NULL,
	thall INT NOT NULL,
	output BOOL NOT NULL
);

COMMIT;

EOF

generate_sql() {
	ROW=$1

	min="SELECT MIN($ROW) AS min_$ROW FROM original"
	max="SELECT MAX($ROW) AS max_$ROW FROM original"

	min="($min)"
	max="($max)"

	printf '%s\n' "(
		CAST($ROW - $min AS REAL) /
		CAST($max - $min AS REAL)
	) AS ${ROW}_norm"
}

cat <<EOF
INSERT INTO heart (
	age,
	age_norm,
	sex,
	cp,
	trtbps,
	trtbps_norm,
	chol,
	chol_norm,
	fbs,
	restecg,
	thalachh,
	thalachh_norm,
	exng,
	oldpeak,
	oldpeak_norm,
	slp,
	caa,
	thall,
	output
)
SELECT
	age,
	$(generate_sql age),
	sex,
	cp,
	trtbps,
	$(generate_sql trtbps),
	chol,
	$(generate_sql chol),
	fbs,
	restecg,
	thalachh,
	$(generate_sql thalachh),
	exng,
	oldpeak,
	$(generate_sql oldpeak),
	slp,
	caa,
	thall,
	output
FROM original;

EOF

echo "ok" >&2
