import sqlite3
import matplotlib.pyplot as plt
import seaborn as sns

conn = sqlite3.connect('./heart.db')
cur = conn.cursor()
rows = ['age', 'age_norm', 'sex', 'cp', 'trtbps', 'trtbps_norm', 'chol',
        'chol_norm', 'fbs', 'restecg', 'thalachh', 'thalachh_norm', 'exng',
        'oldpeak', 'oldpeak_norm', 'slp', 'caa', 'thall', 'output']

# importar información desde la base de datos
# separar cada dato según "output"
dataset = {}
for row in rows:
    res = cur.execute(f'SELECT {row} FROM heart')
    dataset[row] = [n[0] for n in res.fetchall()]

    res = cur.execute(f'SELECT {row} FROM heart WHERE output = 0')
    dataset[row + '_0'] = [n[0] for n in res.fetchall()]

    res = cur.execute(f'SELECT {row} FROM heart WHERE output = 1')
    dataset[row + '_1'] = [n[0] for n in res.fetchall()]

    res = cur.execute(f'SELECT DISTINCT {row} FROM heart')
    dataset[row + '_distinct'] = [n[0] for n in res.fetchall()]


# graficar "output" 0 y 1 en la misma imágen
i = 0
for row in rows:
    nombre = f'hist_separado_{i:02d}_{row}.png'
    data = [dataset[row + '_0'], dataset[row + '_1']]
    print(f'dibujando {nombre}...')

    plt.title(row)
    sns.histplot(data, kde=True, bins=16)
    plt.savefig(nombre)
    plt.clf()

    i = i + 1

# graficar todo junto
i = 0
for row in rows:
    nombre = f'hist_junto_{i:02d}_{row}.png'
    data = dataset[row]
    distinct = len(dataset[row + '_distinct'])
    print(f'dibujando {nombre}...')

    # una imágen bien pequeña
    plt.figure(figsize=(2.9, 2), dpi=100)
    plt.title(row)
    plt.ylabel(' ')
    if distinct <= 5:
        sns.histplot(data, bins=distinct)
    else:
        sns.histplot(data)
    plt.savefig(nombre)
    plt.clf()

    i = i + 1
