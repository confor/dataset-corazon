import csv
import json

schemas = json.load(open('./schema.json', 'r'))
reader = csv.DictReader(open('./heart.csv', 'r'), delimiter=',')


def fix_type(row, schema):
    t = schema['type']

    if t == 'int':
        return int(row)
    elif t == 'double':
        return float(row)
    elif t == 'bool':
        return bool(row)
    else:
        raise ValueError(f'unknown type {t}')


def is_in_range(value, a, b) -> bool:
    return a <= value <= b


def check(value, schema) -> bool:
    name = schema['name']

    if schema['type'] == 'bool':
        if type(value) != bool:
            raise ValueError(f'el dato {name} de valor {value} no es un booleano')

        return True

    elif 'range' in schema:
        range = schema['range']

        if not is_in_range(value, range[0], range[1]):
            raise ValueError(f'el dato {name} de valor {value} está fuera del rango esperado ({range[0]} hasta {range[1]})')

        return True

    elif 'values' in schema:
        values = schema['values']

        if not value in values:
            raise ValueError(f'el dato {name} de valor {value} tiene un valor inesperado, sólo se admiten ({", ".join(map(str, values))})')

        return True

    else:
        return False


print('revisando datos de heart.csv')

for row in reader:
    for schema in schemas:
        name = schema['name']
        value = fix_type(row[name], schema)
        check(value, schema)


print('estan todos los datos dentro de los rangos esperados')
