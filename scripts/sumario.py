import csv
import numpy

file = open('./heart.csv', 'r')
reader = csv.DictReader(file, delimiter=',')
counts = {}

for row in reader:
    for key in row:
        if key not in counts:
            counts[key] = []

        if '.' in row[key]:
            counts[key].append(float(row[key]))
        else:
            counts[key].append(int(row[key]))


file.close()

#######
summary = ['key,min,max,stdev,average,median']

for key in counts:
    data = counts[key]

    data = [
        key,
        min(data),
        max(data),
        round(numpy.std(data), 4),
        round(numpy.average(data), 4),
        numpy.median(data),
    ]

    summary.append(','.join(map(str, data)))

summary = '\n'.join(summary)

print('guardando en summary.csv...')
with open('summary.csv', 'w') as file:
    file.write(summary)

#######

i = 0
j = 0
k = 0
csv = []

for k in range(len(counts) + 1):
    csv.append([])

csv[0].append('-')
for key in counts:
    csv[0].append(key)

for keyX in counts:
    i = i + 1
    j = 0

    csv[i].append(keyX)

    for keyY in counts:
        j = j + 1

        if i == j:
            corr = 1
        else:
            corr = round(numpy.corrcoef(counts[keyX], counts[keyY])[0][1], 3)

        # print(f'{i}, {j} = {keyX}_{keyY} = {corr}')
        csv[i].append(corr)


for idx in range(len(csv)):
    csv[idx] = ','.join(map(str, csv[idx]))

csv = '\n'.join(csv)

print('guardando en corrcoef.csv...')
with open('corrcoef.csv', 'w') as file:
    file.write(csv)
