dataset-corazon
---------------
Repositorio para almacenar scripts relacionados a "Heart Attack Analysis & Prediction Dataset: A dataset for heart attack classification"

Sobre los archivos
------------------
requirements.txt --> Paquetes necesarios (Python 3)
heart.csv --> El dataset original.
dataset.json --> Esquema. Describe los atributos que se tienen en el data set con rangos sensibles.
scripts/import.sh --> Importa heart.csv a una base de datos sqlite3 para facilitar el trabajo.
scripts/validar.py --> Revisa y valida heart.csv (según dataset.json). Falla si es que hay datos inválidos o inesperados.
scripts/graficar.py --> Dibujar histogramas de cada columna del dataset
